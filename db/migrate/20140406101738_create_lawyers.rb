class CreateLawyers < ActiveRecord::Migration
  def change
    create_table :lawyers do |t|
      t.string :code
      t.string :name
      t.integer :yoe
      t.string :location
      t.float :avg_rating		
      t.timestamps
    end
  end
end
