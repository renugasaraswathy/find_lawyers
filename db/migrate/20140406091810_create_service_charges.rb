class CreateServiceCharges < ActiveRecord::Migration
  def change
    create_table :service_charges do |t|
      t.integer :service_id
      t.integer :lawyer_id
      t.integer :charges	
      t.timestamps
    end
  end
end
