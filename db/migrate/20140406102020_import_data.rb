class ImportData < ActiveRecord::Migration
	require 'csv'
  def self.up
		csv_service = File.read('lib/services.csv')		
		services = CSV.parse(csv_service, :headers => false)	
		services.each do |row|			
		  Service.create(:code=>row[0],:name=>row[1])
		end
		puts "Services Imported"

		csv_lawyers = File.read('lib/lawyers.csv')		
		lawyers = CSV.parse(csv_lawyers, :headers => false)	
		lawyers.each do |row|			
		  Lawyer.create(:code=>row[0],:name=>row[1],:yoe=>row[2].to_i,:location=>row[3],:avg_rating=>row[4].to_f)
		end
		puts "Lawyers Imported"	

		csv_lawyers_services=File.read('lib/lawyers_services.csv')		
		service_charges=CSV.parse(csv_lawyers_services, :headers => false)	
		service_charges.each do |row|
			lawyer=Lawyer.find_by_code(row[0])
			service=Service.find_by_code(row[1])
			
			if lawyer and service
				ServiceCharge.create(:lawyer_id=>lawyer.id,:service_id=>service.id,:charges=>row[2].to_i)
			else
				"Lawyer or service Record not found"
			end
		end
		puts "Service Charges Imported"

  end

  def self.down
  	Service.delete_all
  	Lawyer.delete_all
  	ServiceCharge.delete_all
  end

end
