class Service < ActiveRecord::Base
	has_many :service_charges
	has_many :lawyers,:through=>:service_charges
end
