class LawyersController < ApplicationController
	

	def search

		if params[:location] and params[:service] and params[:location]!="" and params[:service]!=""
			@searched_location=params[:location]			
			city=params[:location]
			service_id=params[:service]
			@service=Service.find_by_id(service_id)
			@searched_service=@service.name
			@lawyers=@service.lawyers.where(:location=>city)
		elsif params[:location]
			@searched_location=params[:location]
			@lawyers=Lawyer.where(:location=>@searched_location)			
		end
		@locations=Lawyer.uniq.pluck(:location)
		@services=Service.all
		render :action=>'search'
	end


	def fetch_services		
		@lawyers=Lawyer.select(:id).where(:location=>params[:location])
		@lawyer_ids=""
		@lawyers.each do |lawyer|
			if lawyer==@lawyers.last
			 @lawyer_ids+="#{lawyer.id}"	
			else
			 @lawyer_ids+="#{lawyer.id},"
		    end		
		end
		puts @lawyer_ids
  		@services=Service.find_by_sql("select distinct(service_id) as id,name from services,service_charges where service_charges.service_id=services.id and service_charges.lawyer_id in (#{@lawyer_ids})")
  		render :json => @services.to_json
	end

end
