# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FindLawyers::Application.config.secret_key_base = 'c31c15181faf73f3767f6f72e5c29f4b7a2fa8e72821c44d02418ad6ff6ac02c0bdabd95e7340fde810e36d2b0309bd11ac3722118d95f6419f8408ab2e57066'
